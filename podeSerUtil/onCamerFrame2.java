@Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();


        Imgproc.cvtColor(mRgba,imgGray,Imgproc.COLOR_RGB2GRAY);//CINZA
        Imgproc.blur(imgGray,imgCanny,new Size(5,5));
//        Imgproc.Canny(imgCanny,imgGray,75,200);
//        Imgproc.cvtColor(imgCanny, imgGray, Imgproc.COLOR_BGR2HSV);
        Imgproc.Canny(imgCanny,outputImg,50,100);//CONTORNOS


//        Imgproc.threshold(imgCanny,inputImg,100,125,THRESH_BINARY);//REMOVE RUIDO

//        Core.inRange(imgGray, new Scalar(58,125,0), new Scalar(256,256,256), imgGray);
//        Mat erode = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3));
//        Mat dilate = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5,5));
//        Imgproc.erode(imgGray, outputImg, erode);
//        Imgproc.erode(imgGray, outputImg, erode);
//
//        Imgproc.dilate(imgGray, outputImg, dilate);
//        Imgproc.dilate(imgGray, outputImg, dilate);
//
//        List<MatOfPoint> contours = new ArrayList<>();
//
//        Imgproc.findContours(outputImg, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
//        Imgproc.drawContours(outputImg, contours, -1, new Scalar(255,255,0));

//        Mat mRgbaT = imgGray.t(); //ROTACIONAR
        Core.flip(outputImg.t(), outputImg, 1); //ROTACIONA
        Imgproc.resize(outputImg, outputImg, mRgba.size());

        Log.d(TAG,mRgba.size().toString());

//        Mat mRgbaT = mRgba.t(); //ROTACIONAR
//        Core.flip(mRgba.t(), mRgbaT, 1); //ROTACIONA
//        Imgproc.resize(mRgbaT, mRgbaT, outputImg.size());//REDIMENCIONA PARA EXIBIR
        return outputImg;
    }