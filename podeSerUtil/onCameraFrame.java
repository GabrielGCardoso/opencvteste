 public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        Mat mRgbaT = mRgba.t(); //ROTACIONAR
        Core.flip(mRgba.t(), mRgbaT, 1); //ROTACIONA

        Imgproc.cvtColor(mRgbaT,imgGray,Imgproc.COLOR_RGB2GRAY);//CINZA
        Imgproc.Canny(imgGray,imgCanny,50,100);//CONTORNOS

        Imgproc.threshold(imgCanny,inputImg,100,125,THRESH_BINARY);//REMOVE RUIDO

        Core.inRange(inputImg, new Scalar(58,125,0), new Scalar(256,256,256), mRgbaT);
        Mat erode = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3));
        Mat dilate = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5,5));
        Imgproc.erode(inputImg, outputImg, erode);
        Imgproc.erode(inputImg, outputImg, erode);

        Imgproc.dilate(inputImg, outputImg, dilate);
        Imgproc.dilate(inputImg, outputImg, dilate);

        List<MatOfPoint> contours = new ArrayList<>();

        Imgproc.findContours(outputImg, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        Imgproc.drawContours(outputImg, contours, -1, new Scalar(255,255,0));

        Imgproc.resize(mRgbaT, mRgbaT, outputImg.size());//REDIMENCIONA PARA EXIBIR

        Log.d(TAG,outputImg.dump());


        return outputImg;
//        Mat mRgbaT = mRgba.t();
//        Core.flip(mRgba.t(), mRgbaT, 1);
//        Imgproc.resize(mRgbaT, mRgbaT, mRgba.size());
//        return mRgbaT;

//        Imgproc.cvtColor(mRgba,imgGray, Imgproc.COLOR_BGR2HSV);
//        Mat dest = new Mat();
//        // Mat dest = new Mat(src.width(), src.height(), src.type());
//        Core.inRange(imgGray, new Scalar(58,125,0), new Scalar(256,256,256), dest);
//
//        Mat erode = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3));
//        Mat dilate = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5,5));
//        Imgproc.erode(dest, dest, erode);
//        Imgproc.erode(dest, dest, erode);
//
//        Imgproc.dilate(dest, dest, dilate);
//        Imgproc.dilate(dest, dest, dilate);
//
//        List<MatOfPoint> contours = new ArrayList<>();
//        Imgproc.findContours(dest, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
//        Imgproc.drawContours(dest, contours, -1, new Scalar(255,255,0));

